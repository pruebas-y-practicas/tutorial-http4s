name := "http4s-tutorial"

version := "0.1"

scalaVersion := "2.13.6"

val http4s = "1.0.0-M23"
val circe = "0.14.1"

libraryDependencies ++= Seq(
  "org.http4s" %% "http4s-blaze-server" % http4s,
  "org.http4s" %% "http4s-circe" % http4s,
  "org.http4s" %% "http4s-dsl" % http4s,
  "io.circe" %% "circe-generic" % circe
)
