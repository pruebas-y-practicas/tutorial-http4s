package com.eduardoobando

import com.eduardoobando.Classes._

import java.util.UUID
import scala.collection.mutable

object Data {

  private val dic1 = Director("Zack", "Snyder")
  private val snjl: Movie = Movie(
    "6bcbca1e-efd3-411d-9f7c-14b872444fce",
    "Zack Snyder's Justice League",
    2021,
    List("Henry Cavill", "Gal Godot", "Ezra Miller", "Ben Affleck", "Ray Fisher", "Jason Momoa"),
    dic1.toString
  )

  private val directors: mutable.Map[Director, DirectorDetails] =
    mutable.Map(dic1 -> DirectorDetails.fromDirector(dic1)("superhero"))

  private val movies: Map[String, Movie] = Map(snjl.id -> snjl)

  def findDirectorDetailsByDirector(director: Director): Option[DirectorDetails] = directors.get(director)
  def findMovieById(movieID: UUID): Option[Movie] = movies.get(movieID.toString)
  def findMoviesByDirector(director: String): List[Movie] = movies.values.filter(_.director == director).toList
}
